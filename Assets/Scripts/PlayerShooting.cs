using System;
using UnityEngine;

public class PlayerShooting : MonoBehaviour {
    public                   GameObject objBullet;
    public                   float      timeBulletSpawn = 0.3f;
    [HideInInspector] public float      timerShoot;

    private void Update() {
        if(Time.time > timerShoot) {
            timerShoot = Time.time + timeBulletSpawn;
            Instantiate(objBullet, transform.position, Quaternion.identity);
        }
    }
}
