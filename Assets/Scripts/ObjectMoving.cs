﻿using UnityEngine;

public class ObjectMoving : MonoBehaviour {
	[SerializeField] public float speed;

    private void Update() {
    transform.Translate(Vector3.up * speed * Time.deltaTime);    
    }
}
