﻿using UnityEngine;

public class DestroyAll : MonoBehaviour {
	private BoxCollider2D boundareCollider;
	private Vector2       viewPortSize;

	private float boxColliderScale = 3.5f;

	private void Awake() {
		boundareCollider = GetComponent<BoxCollider2D>();
	}

	private void Start() {
		ResizeCollider();
	}

	private void ResizeCollider() {
		boundareCollider.size = Camera.main.ViewportToWorldPoint(new Vector2(1, 1)) * boxColliderScale;
	}

	private void OnTriggerExit2D(Collider2D other) {
		switch(other.tag) {
			case "Planet" :
				Destroy(other.gameObject);
				break;
		}
	}
}
