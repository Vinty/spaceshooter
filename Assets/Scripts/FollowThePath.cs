﻿using System.Linq;
using UnityEngine;

public class FollowThePath : MonoBehaviour {
	[HideInInspector] public Transform[] pathPoints;
	[HideInInspector] public float       speedEnemy;
	[HideInInspector] public bool        isReturn;
	[HideInInspector] public Vector3[]   newPosition;

	private int curPos;

	private void Start() {
		newPosition        = NewPositionByPath(pathPoints);
		transform.position = newPosition[0];
	}

	private void Update() {
		transform.position = Vector3.MoveTowards(transform.position, newPosition[curPos], speedEnemy * Time.deltaTime);
		if(Vector3.Distance(transform.position, newPosition[curPos]) < 0.2f) {
			curPos++;
			if(isReturn && Vector3.Distance(transform.position, newPosition[newPosition.Length - 1]) < 0.3f) {
				curPos = 0;
			}
		}

		if(Vector3.Distance(transform.position, newPosition[newPosition.Length - 1]) < 0.2f && !isReturn) {
			Destroy(gameObject);
		}
	}

	private Vector3[] NewPositionByPath(Transform[] pathPos) {
		Vector3[] pathPositionWithSmooth = pathPos.ToList().Select(x => x.position).ToArray();
		return Smoothing(Smoothing(Smoothing(pathPositionWithSmooth)));
	}

	private Vector3[] Smoothing(Vector3[] pathPositions) {
		Vector3[] newPathPositions = new Vector3[(pathPositions.Length - 2) * 2 + 2];
		newPathPositions[0]                           = pathPositions[0];
		newPathPositions[newPathPositions.Length - 1] = pathPositions[pathPositions.Length - 1];

		int j = 1;
		for(int i = 0; i < pathPositions.Length - 2; i++) {
			newPathPositions[j] = pathPositions[i] + (pathPositions[i + 1] - pathPositions[i]) * 0.75f;
			newPathPositions[j + 1] = pathPositions[i + 1] + (pathPositions[i + 2] - pathPositions[i + 1]) * 0.25f;
			j += 2;
		}

		return newPathPositions;
	}
}
