﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class Enemy : MonoBehaviour {
	public int enemyHealth;

	[Space] public GameObject objBullet;

	public float shotTimeMin, shotTimeMax;
	public int   shotChance;

	private void Start() {
		Invoke("OpenFire", Random.Range(shotTimeMin, shotTimeMax));
	}

	private void OpenFire() {
		if(Random.value < (float) shotChance / 100) {
			Instantiate(objBullet, transform.position, Quaternion.identity);
		}
	}

	public void GetDamage(int damage) {
		enemyHealth -= damage;
		if(enemyHealth <= 0) {
			Destruction();
		}
	}

	private void Destruction() {
		Destroy(gameObject);
	}

	private void OnTriggerEnter2D(Collider2D other) {
		if(other.tag == "Player") {
			GetDamage(1);
			Player.instance.GetDamage(1);
		}
	}
}
