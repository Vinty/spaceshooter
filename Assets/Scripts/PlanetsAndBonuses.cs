﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlanetsAndBonuses : MonoBehaviour {
	public GameObject[] objPlanets;
	public float        timePlanetSpawn;
	public float        speedPlanes;

	private List<GameObject> planetliList = new List<GameObject>();
	private Coroutine        planetCoroutine;

	private void Update() {
		if(planetliList.Count == 0) {
			planetliList = objPlanets.ToList();
		}

		if(planetCoroutine != null) {
			return;
		}

		planetCoroutine = StartCoroutine(PlanetCreation());
	}

	private IEnumerator PlanetCreation() {
		yield return new WaitForSeconds(7);
		while(true) {
			int        randomIndex = Random.Range(0, planetliList.Count);
			GameObject newPlanet   = CreatePlanet(randomIndex);
			newPlanet.GetComponent<ObjectMoving>().speed = speedPlanes;

			planetliList.RemoveAt(randomIndex);
			yield return new WaitForSeconds(timePlanetSpawn);
		}
	}

	private GameObject CreatePlanet(int randomIndex) {
		Vector3 planetPosition = new Vector3(
			Random.Range(PlayerMoving.instance.borders.minX, PlayerMoving.instance.borders.maxX),
			PlayerMoving.instance.borders.maxY * 1.5f, 0
		);
		Quaternion planetQuaternion = Quaternion.Euler(0, 0, Random.Range(-25, 25));
		return Instantiate(planetliList[randomIndex], planetPosition, planetQuaternion);
	}
}
