﻿using UnityEngine;

public class RepitingBG : MonoBehaviour {
	// высота спрайта должна быть выше высоты камеры
	// лучше использовать box-collider
	public float verticalSize;

	private Vector2 offSetUp;
	private int     spriteCount = 2;

	private void Update() {
		if(transform.position.y < -verticalSize) {
			RepeatBackground();
		}
	}

	private void RepeatBackground() {
		offSetUp          = new Vector2(0, verticalSize * spriteCount);
		transform.position = (Vector2) transform.position + offSetUp;
	}
}
