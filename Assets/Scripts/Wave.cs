using System;
using System.Collections;
using System.Linq;
using UnityEditor;
using UnityEngine;

[Serializable]
public class ShootingSetting {
	[Range(0, 100)] public int shotChance;

	public float shotTimeMin, shotTimeMax;
}


public class Wave : MonoBehaviour {
	public GameObject  objEnemy;
	public int         countInWave;
	public float       speedEnemy;
	public float       timeSpawn;
	public Transform[] pathPoint;
	public bool        isReturn;

	[Space] [Header("Для теста волны")] public bool isTestWave;

	public  ShootingSetting shootingSetting;
	private Enemy           enemyComponentScript;
	private FollowThePath   followThePathComponent;

	private void Start() {
		StartCoroutine(CreateEnemyWave());
	}

	private IEnumerator CreateEnemyWave() {
		for(int i = 0 ;
			i < countInWave ;
			i++) {
			GameObject newEnemy = Instantiate(objEnemy, objEnemy.transform.position, Quaternion.identity);
			followThePathComponent            = newEnemy.GetComponent<FollowThePath>();
			followThePathComponent.pathPoints = pathPoint;
			followThePathComponent.speedEnemy = speedEnemy;
			followThePathComponent.isReturn   = isReturn;

			enemyComponentScript             = newEnemy.GetComponent<Enemy>();
			enemyComponentScript.shotChance  = shootingSetting.shotChance;
			enemyComponentScript.shotTimeMin = shootingSetting.shotTimeMin;
			enemyComponentScript.shotTimeMax = shootingSetting.shotTimeMax;
			newEnemy.SetActive(true);
			yield return new WaitForSeconds(timeSpawn);
		}

		if(isTestWave) {
			yield return new WaitForSeconds(5);
			StartCoroutine(CreateEnemyWave());
		}

		if(!isReturn) {
			Destroy(gameObject);
		}
	}

	private void OnDrawGizmos() {
		NewPositionByPath(pathPoint);
	}

	private void NewPositionByPath(Transform[] path) {
		Vector3[] pathPositions = path.ToList().Select(x => x.position).ToArray();

		pathPositions = Smoothing(pathPositions);
		pathPositions = Smoothing(pathPositions);
		pathPositions = Smoothing(pathPositions);
		
		for(int i = 0; i < pathPositions.Length - 1; i++) {
			Gizmos.DrawLine(pathPositions[i], pathPositions[i + 1]);
		}
	}

	private Vector3[] Smoothing(Vector3[] pathPositions) {
		Vector3[] newPathPositions = new Vector3[(pathPositions.Length - 2) * 2 + 2];
		newPathPositions[0]                           = pathPositions[0];
		newPathPositions[newPathPositions.Length - 1] = pathPositions[pathPositions.Length - 1];

		int j = 1;
		for(int i = 0; i < pathPositions.Length - 2; i++) {
			newPathPositions[j] = pathPositions[i] + (pathPositions[i + 1] - pathPositions[i]) * 0.75f;
			newPathPositions[j + 1] = pathPositions[i + 1] + (pathPositions[i + 2] - pathPositions[i + 1]) * 0.25f;
			j += 2;
		}

		return newPathPositions;
	}
}
