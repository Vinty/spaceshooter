using UnityEngine;

public class Bullet : MonoBehaviour {
	public int  damage;
	public bool isEnemyBullet;

	private void Destruction() {
		Destroy(gameObject);
	}

	private void OnTriggerEnter2D(Collider2D other) {
		if(isEnemyBullet && other.tag.Equals("Player")) {
			Player.instance.GetDamage(damage);
			Destruction();
		}
		else if(!isEnemyBullet && other.tag.Equals("Enemy")) {
			other.GetComponent<Enemy>().GetDamage(damage);
			Destruction();
		}
	}
}
