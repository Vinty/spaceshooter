﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable] public class Borders {
    public                   float minXOffset = 1.1f;
    public                   float minYOffset = 1.1f;
    public                   float maxXOffset = 1.1f;
    public                   float maxYOffset = 1.1f;
    
    [HideInInspector] public float minX, maxX, minY, maxY;
}

public class PlayerMoving : MonoBehaviour {
    public static PlayerMoving instance;
    public        Borders      borders;

    public  int     speedPlayer = 5;
    private Camera  camera;
    private Vector2 mousePosition;

    private void Awake() {
        if(instance == null) {
            instance = this;
        }
        else {
            Destroy(gameObject);
        }
        
        camera = Camera.main;
    }

    private void Start() {
        ResizeBorder();
        
    }

    private void Update() {
        if(Input.GetMouseButton(0)) {
            // get 2d coordinate(xy) click on screen
            mousePosition     = camera.ScreenToWorldPoint(Input.mousePosition);
            transform.position = Vector2.MoveTowards(transform.position, mousePosition, speedPlayer * Time.deltaTime);
        }
        
        transform.position = new Vector2(
            Mathf.Clamp(transform.position.x, borders.minX, borders.maxX), 
            Mathf.Clamp(transform.position.y, borders.minY, borders.maxY));
    }

    private void ResizeBorder() {
        // border left
        borders.minX = camera.ViewportToWorldPoint(Vector2.zero).x + borders.minXOffset;
        //border bottom
        borders.minY = camera.ViewportToWorldPoint(Vector2.zero).y + borders.minYOffset;
        // border right
        borders.maxX = camera.ViewportToWorldPoint(Vector2.right).x - borders.maxXOffset;
        //border top
        borders.maxY = camera.ViewportToWorldPoint(Vector2.up).y - borders.maxYOffset;
    }
}
