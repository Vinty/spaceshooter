using UnityEngine;

public class Player : MonoBehaviour {
    public static Player instance = null;

    public int playerHealth = 1;

    private void Awake() {
        if(instance ==  null) {
            instance = this;
        }
        else {
            Destroy(gameObject);
        }
    }

    public void GetDamage(int damage) {
        playerHealth -= damage;
        if(playerHealth <=0) {
            Destruction();
        }
    }

    private void Destruction() {
        Destroy(gameObject);
    }
}
