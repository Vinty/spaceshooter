using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class EnemyWaves {
	public float      timeToStart;
	public GameObject wave;
	public bool       isLastWave;
}

public class LevelController : MonoBehaviour {
	public static LevelController instance;
	public        GameObject[]    playerShip;
	public        EnemyWaves[]    enemyWaves;

	private void Awake() {
		if(instance == null) {
			instance = this;
		}
		else {
			Destroy(gameObject);
		}
	}

	private void Start() {
		foreach(EnemyWaves wave in enemyWaves) {
			StartCoroutine(CreateEnemyWave(wave.timeToStart, wave.wave));
		}
	}

	private IEnumerator CreateEnemyWave(float delay, GameObject wave) {
		if(delay != null) {
			yield return new WaitForSeconds(delay);
		}

		if(Player.instance != null) {
			Instantiate(wave);
		}
	}
}
    
